[size=15][b]Simple Theme[/b][/size]

This is a lightweight dark theme using mostly the same colors of another theme of mine: [url=https://www.pling.com/p/1517069/]Soil[/url]. It is dark, with green accents. Disabled elements are brown. It is very easy on the eyes. It looks very much like old desktop themes from the nineties (somewhat like a dark Windows 95 theme). It is not the goal of this theme, but the effect is there anyway.

It is a skeuomorphic theme, but with square corners and no gradients, in order to make it as lightweight as possible. I've reduced to the minimum the use of boxshadows, with mostly all 3D aspect generated with borders, which I guess are faster to draw. There are also no animations or transitions, although there are hover effects. CSS size is relatively small compared to other themes.

Made with [url=https://github.com/Elbullazul/Azurra_framework]Azurra Framework by Elbullazul[/url]. Source code for my theme is also included.

I've tested it only under XFCE, but I'd say it should work also with any other GTK desktop. I've tried Nautilus and Gedit.

This themes includes the following elements:
GTK2, GTK3, metacity, openbox, Unity, XFWM4, Unity, KDE Color scheme and QT5ct color scheme. 
Metacity, openbox, Unity, Cinammon and QT5ct color schemes were create using Oomox and then modified by me. GTK2 theme modified from [url=https://www.gnome-look.org/p/1404891]Nexto theme[/url].

Xfwm4 theme (that is, Xfce window decorations) is entirely done by me, and should adapt its colors to any other GTK theme, so it is very versatile in itself.

[size=13][b] Integration with other DE[/b][/size]

To integrate this theme with Plasma or QT:

- Plasma: import the color scheme in KDE_Plasma_colors folder. I recommend using Oxygen widget (application) style instead of Breeze, or QtCurve.
- Other QT desktops: copy Soil.conf in KDE_Plasma_colors to ~/.config/qt5ct/colors (to activate it, you need to use qt5ct to manage the aspect of your QT applications)

For Qt6 apps, I'm sorry, there is no Kvantum theme that adapts properly, and QtCurve doesn't seem to work there. Also, there is still no GTK4 support for this theme.

[size=13][b]Donations[/b][/size]
I've decided to add an option to donate money: If I had enough time and income, I'd definitely dedicate much more time to this activity, which I enjoy very much. Thank you!

Here's the [url=https://paypal.me/nestorandreu]Paypal link[/url].