# Simple Theme

![square preview](preview/square.png)

This is a lightweight dark theme using mostly the same colors of another theme of mine: [Soil](https://www.pling.com/p/1517069/). It is dark, with green accents. Disabled elements are brown. It is very easy on the eyes. It looks very much like old desktop themes from the nineties (somewhat like a dark Windows 95 theme). It is not the goal of this theme, but the effect is there anyway. The main goals are: very lightweight, dark but easy on the eyes theme, UI elements have a very clear legibility as for their function, works well both for compositing and non-composting window managers, ideal for old hardware but perfectly usable in modern too.

It is a skeuomorphic theme, but with square corners and no gradients, in order to make it as lightweight as possible. I've reduced to the minimum the use of boxshadows, with mostly all 3D aspect generated with borders, which I guess are faster to draw. There are also no animations or transitions, although there are hover effects.

GTK3 Made with [Azurra Framework by Elbullazul](https://github.com/Elbullazul/Azurra_framework). Source code for my theme is also included.

For GTK3: I've tested it only under XFCE, but I'd say it should work also with any other GTK desktop. I've tried Nautilus and Gedit.

This themes includes the following elements:

- GTK2 (not exactly a perfect port, but it tries to look a bit like it)
- GTK3 (no GTK4 for now...)
- metacity
- openbox
- Unity
- XFWM4
- KDE Color scheme
- Qt5ct and Qt6ct color scheme
- qtcurve configuration (attempt to make it the most similar possible, within qtcurves limitations)
- Kvantum theme (much better to use Kvantum than qtcurve as widget style; moreover, Kvantum works for qt6)
- Plasma theme (note: although it says it adapts to color themes, some elements have their color hardcoded, so for now better only use it with Simple color scheme)
- Aurorae theme (KWin (that is, KDE Plasma's default window manager) window decoration.

Metacity, openbox, Unity, Cinammon and QtNct color schemes were create using Oomox and then modified by me. GTK2 theme modified from [Nexto theme](https://github.com/darkomarko42/Nexto).

Xfwm4 theme (that is, Xfce window decorations) is entirely done by me, and should adapt its colors to any other GTK theme, so it is very versatile in itself.
Kvantum and Aurorae themes also made by me from scratch. Plasma theme adapted from Breeze.

## Integration with other DE

To integrate this theme with Plasma or QT. You can either download corresponding Simple themes using Plasma Configuration tool, or, if you prefer to use the cloned repo, then:

- Plasma: 
  - import the color scheme in `kde_plasma_qt/plasma_colorscheme/` folder.
  - With Kvantum manager, import kde_plasma_qt/Kvantum/Simple/`
  - create a symbolic link to `kde_plasma_qt/aurorae/Simple/` called `Simple` at `~/.local/share/aurorae/themes`
- Other QT desktops: 
  - copy Simple.conf in `kde_plasma_qt/qt5_or_6ct_color_scheme/` to `~/.config/qt5ct/colors` (to activate it, you need to use qt5ct or qt6ct to manage the aspect of your QT applications) 
  - Do the same as with Plasma for Kvantum

(note: you can also use the strategy of creating symbolic links for Kvantum and color scheme).

For GTK, metacity, xfwm4, openbox and Unity, just copy the root folder (or create a symbolic link to it) in `~/.themes` (or, if you want it to be available to all users, `/usr/share/themes`).


## Donations
I've decided to add an option to donate money: If I had enough time and income, I'd definitely dedicate much more time to this activity, which I enjoy very much. Thank you!

Here's the [Paypal link](https://paypal.me/nestorandreu).

## Previews

### GTK 2 and 3 ###

![preview 1](preview/full-gtk3wf1-awf_gtk2.png "gtk3-widget-factory, awf with gtk2")
![preview 2](preview/gtk3wf-2.png "gtk3-widget-factory page 2")
![preview 3](preview/gtk3wf-3.png "gtk3-widget-factory page 3")
![preview 4: File picker](preview/File_picker.png "File picker preview")

### Kvantum theme ###

![preview 1](preview/kvantum-buttons.png "Kvantum buttons")
![preview 2](preview/kvantum-combos_edits.png "Kvantum combos and line edits")
![preview 3](preview/kvantum-containers.png "Kvantum containers")
![preview 4](preview/kvantum-radios_checks.png "Kvantum radios and checks")
![preview 5](preview/kvantum-sliders.png "Kvantum sliders")

### Plasma theme ###

![preview 1](preview/plasma_full_desktop.jpg "Plasma full desktop")
![preview 2](preview/plasma_launcher.png "Plasma application launcher")
![preview 3](preview/plasma_volume_panel.png "Plasma volume panel")
